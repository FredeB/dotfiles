#devtools
alias ec="emacsclient -c . &"
alias spi="ssh root@10.9.8.2"
alias stm="/usr/local/STMicroelectronics/STM32Cube/STM32CubeMX/STM32CubeMX"
alias gs="git status"

#spotify
alias tspar="tizonia --spotify-artist"
alias tspal="tizonia --spotify-album"
alias tsppl="tizonia --spotify-playlist"

#navigation
alias la="ls -lah"

#unsorted
alias chat="ssh bun@chat.shazow.net"
alias mouse="sudo modprobe -r psmouse && sudo modprobe psmouse"
alias z="zathura"
alias r="ranger ."
alias inst="sudo apt install -y"
alias remo="sudo apt autoremove -y"
alias upgr="sudo apt update && sudo apt upgrade -y"
alias gl="git log --graph"
alias gd="git diff"
alias gp="git pull"
alias c="clear"
alias sus="sudo sh -c 'echo mem > /sys/power/state' &"
alias remacs="pkill emacs && emacs --daemon"
alias cd..="cd .."
alias n="nnn"
alias et="emacsclient -t ."
